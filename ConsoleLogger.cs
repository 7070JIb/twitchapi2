﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPIFramework
{
    public class ConsoleLogger : ILogger
    {

        public void Log(List<Tuple<string, ConsoleColor>> messagesColoured)
        {
            foreach (var messageAndColor in messagesColoured)
            {
                Console.BackgroundColor = messageAndColor.Item2;
                Console.Write(messageAndColor.Item1);
                Console.BackgroundColor = ConsoleColor.Black;
            }
            Console.WriteLine();
        }

        public void Log(string message, ConsoleColor color = ConsoleColor.Black)
        {
            Console.BackgroundColor = color;
            Console.Write(message);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
