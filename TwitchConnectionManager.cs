﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
//using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;
using System.Resources;
using System.Reflection;

namespace TwitchAPIFramework
{
    public class TwitchConnectionManager
    {
        // Берем из Resources
        static string CLIENT_ID;
        static string CLIENT_SECRET;
        static string STATE_PASSWORD;

        // Для удобства выносим некоторые строки в отдельные переменные
        const string REDIRECT_URI = "http://localhost:8080";
        const string TAGET_SCOPES = "analytics:read:extensions%20analytics:read:games%20bits:read%20channel:read:subscriptions%20user:edit%20chat:edit%20chat:read%20whispers:read%20whispers:edit";
        const string TWITCH_API_URI_BASE = "https://api.twitch.tv/helix/";

        // Получаем из запросов
        static string interCode;
        static string accessToken;
        static string refreshToken;

        //Если true, то токены берутся из локального файла, если false, то отправляются запросы токенов
        // Для дебаггинга
        static bool ToFormatMessages = true;


        static HttpListener httpListener;

        static int validationExpiresIn = 1000000;
        static int accessTokenExpiresIn = 1000000;

        public ChatBot Bot { get; private set; }
        public static ILogger MyLogger;

        public TwitchConnectionManager(ILogger logger, bool IsAuthorized)
        {
            MyLogger = logger;
            //defe
            // Подключение к ресурсам для доступа к Client Secret
            ResourceManager resources = new ResourceManager("Resources", Assembly.GetExecutingAssembly());


            CLIENT_SECRET = Properties.Resources.CLIENT_SECRET;
            CLIENT_ID = Properties.Resources.CLIENT_ID;
            STATE_PASSWORD = Properties.Resources.STATE_PASSWORD;

            // Запуск прослушки на локальном сервере
            httpListener = new HttpListener();
            httpListener.Prefixes.Add(REDIRECT_URI + "/");
            httpListener.Start();
            MyLogger.Log("Старт прослушки на http://localhost:8080");
            if (!IsAuthorized)
            {
                // OAuth Authorization Code Flow	
                GetInterCode();
                GetAccess();
            }
            else
            {
                ReadCodesFromFile();
                RefreshAccessToken();
            }

            Thread revalidationThread = new Thread(ReValidatePeriodically) { IsBackground = true };
            revalidationThread.Start();

            Thread refreshAccessTokenThread = new Thread(RefreshAccessTokenPeriodically) { IsBackground = true };
            refreshAccessTokenThread.Start();

            httpListener.Close();
        }

        private void ReadCodesFromFile()
        {
            using (StreamReader reader = new StreamReader(new FileStream("keys.txt", FileMode.Open, FileAccess.Read)))
            {
                // находим Access Token
                while (true)
                {
                    string[] keyvalue = reader.ReadLine().Split(' ');
                    if (keyvalue[0] == "access_token")
                    {
                        accessToken = keyvalue[1];
                        break;
                    }
                }
                // находим Refresh Token
                while (true)
                {
                    string[] keyvalue = reader.ReadLine().Split(' ');
                    if (keyvalue[0] == "refresh_token")
                    {
                        refreshToken = keyvalue[1];
                        break;
                    }
                }
            }
        }

        public void CreateBot()
        {
            Bot = new ChatBot("model7070", accessToken, ToFormatMessages);
            RequestMembership();
        }

        public void JoinChannel(string channelName) => Bot.JoinChannel(channelName);

        public void SendMessageToChat(string message)
        {
            if (Bot.IsOnChannel)
                Bot.SendMessageToChat(message);
            else MyLogger.Log("tried to send message outside chat", ConsoleColor.Red);
        }

        void RequestMembership()
        {
            Bot.RequestMembership();
            while (!Bot.GotPermissions)
            {
                MyLogger.Log("Wait For permissions...");
                Thread.Sleep(1000);
            }
            MyLogger.Log("Можно управлять ботом");
        }

        private static void ReValidatePeriodically()
        {
            while (true)
            {

                //Ждем до 5 сек до времени истечения подлинности токена
                Thread.Sleep(validationExpiresIn - 5000);
                ReValidate();
            }
        }

        private static void RefreshAccessTokenPeriodically()
        {
            while (true)
            {

                //Ждем до 5 сек до времени истечения подлинности токена
                Thread.Sleep(accessTokenExpiresIn - 5000);
                RefreshAccessToken();
            }
        }

        //class RevalidateRespond : ISerializableJSON
        //{
        //    [JsonProperty("client_id")]
        //    public string ClientId { get; set; }
        //    [JsonProperty("login")]
        //    public string Login { get; set; }
        //    [JsonProperty("scopes")]
        //    public string[] Scopes { get; set; }
        //    [JsonProperty("user_id")]
        //    public string UserId { get; set; }
        //    [JsonProperty("expires_in")]
        //    public int ExpiresIn { get; set; }

        //    public string JSON { get; set; }
        //}

        private static void ReValidate()
        {
            TwitchConnectionManager.MyLogger.Log("ReValidate");
            HttpWebRequest request = WebRequest.CreateHttp("https://id.twitch.tv/oauth2/validate");

            request.Headers.Add("Authorization: OAuth " + accessToken);


            //RevalidateRespond respond = DeserializeResponse<RevalidateRespond>(request);
            string respond = GetJsonResponse(request);

            validationExpiresIn = 3600 * 1000;

            TwitchConnectionManager.MyLogger.Log("VALIDATED WITH RESPOND " + FindInJson(respond, "login"));
            return;
        }

        //class RefreshRespond : ISerializableJSON
        //{
        //    [JsonProperty("access_token")]
        //    public string AccessToken { get; set; }
        //    [JsonProperty("refresh_token")]
        //    public string RefreshToken { get; set; }
        //    [JsonProperty("scope")]
        //    public string[] Scope { get; set; }

        //    public string JSON { get; set; }
        //}

        private static void RefreshAccessToken()
        {
            TwitchConnectionManager.MyLogger.Log("RefreshAccessToken was = " + accessToken);

            HttpWebRequest request = WebRequest.CreateHttp("https://id.twitch.tv/oauth2/token"
                //+ "--data-urlencode"
                + "?grant_type=refresh_token"
                + "&refresh_token=" + refreshToken
                + "&client_id=" + CLIENT_ID
                + "&client_secret=" + CLIENT_SECRET);

            request.Method = "POST";


            //RefreshRespond respond = DeserializeResponse<RefreshRespond>(request);
            string response = GetJsonResponse(request);

            accessToken = FindInJson(response, "access_token");
            accessTokenExpiresIn = 3600 * 1000;
            refreshToken = FindInJson(response, "refresh_token");

            ////Найти старые ключи 
            //using (StreamReader reader = new StreamReader(new FileStream("keys.txt", FileMode.Open, FileAccess.Read)))
            //{

            //    // Hаходим Access Token
            //    accessToken = reader.ReadLine().Split(' ')[1];

            //    // Ищем дальше, находим Refresh Token
            //    refreshToken = reader.ReadLine().Split(' ')[1];

            //}

            // Заменить access token в файле
            using (StreamWriter writer = new StreamWriter(new FileStream("keys.txt", FileMode.Create)))
            {
                writer.WriteLine("access_token " + accessToken);
                writer.WriteLine("refresh_token " + refreshToken);
            }

            TwitchConnectionManager.MyLogger.Log("ACCESS TOKEN UPDATED TO " + accessToken);
        }

        //class SearchChannelResponse : ISerializableJSON
        //{
        //    public class Data
        //    {
        //        [JsonProperty("broadcaster_language")]
        //        public string BroadcasterLanguage { get; set; }
        //        [JsonProperty("broadcaster_login")]
        //        public string BroadcasterLogin { get; set; }
        //        [JsonProperty("display_name")]
        //        public string DisplayName { get; set; }
        //        [JsonProperty("game_id")]
        //        public string GameId { get; set; }
        //        [JsonProperty("id")]
        //        public string Id { get; set; }
        //        [JsonProperty("is_live")]
        //        public bool IsLive { get; set; }
        //        [JsonProperty("tags_ids")]
        //        public string[] TagsIds { get; set; }
        //        [JsonProperty("thumbnail_url")]
        //        public string ThumbnailUrl { get; set; }
        //        [JsonProperty("title")]
        //        public string Title { get; set; }
        //        [JsonProperty("started_at")]
        //        public string StartedAt { get; set; }
        //    }
        //    [JsonProperty("data")]
        //    public Data[] ChannelsData { get; set; }
        //    [JsonIgnore]
        //    public string JSON { get; set; }
        //}

        //private static void SearchChannel(string query)
        //{
        //    HttpWebRequest request = WebRequest.CreateHttp(TWITCH_API_URI_BASE + "search/channels"
        //        + "?query=" + query
        //        + "&first=" + 1);

        //    request.Headers.Add("Authorization: Bearer " + accessToken);
        //    request.Headers.Add("Client-Id: " + CLIENT_ID);

        //    SearchChannelResponse response = DeserializeResponse<SearchChannelResponse>(request);

        //    TwitchConnectionManager.MyLogger.Log("Result for channelSearch \"" + query + "\": " + response.JSON);

        //}


        private static void GetInterCode()
        {
            // Отправка запроса для авторизации
            Process.Start($"https://id.twitch.tv/oauth2/authorize?"
                + $"client_id=" + CLIENT_ID
                + "&redirect_uri=" + REDIRECT_URI
                + "&response_type=code"
                + "&scope=" + TAGET_SCOPES
                + "&state=" + STATE_PASSWORD);

            HttpListenerContext requestContext = httpListener.GetContext();
            HttpListenerRequest requestToServer = requestContext.Request;


            TwitchConnectionManager.MyLogger.Log("Получил запрос: " + requestToServer.HttpMethod + " " + requestToServer.Url.AbsoluteUri + "\n");

            interCode = requestToServer.QueryString.Get("code");
        }


        //class AccessTokenRespond : ISerializableJSON
        //{
        //    [JsonProperty("access_token")]
        //    public string AccessToken { get; set; }
        //    [JsonProperty("refresh_token")]
        //    public string RefreshToken { get; set; }
        //    [JsonProperty("expires_in")]
        //    public int ExpriresIn { get; set; }
        //    [JsonProperty("scope")]
        //    public string[] Scopes { get; set; }
        //    [JsonProperty("token_type")]
        //    public string TokenType { get; set; }

        //    public string JSON { get; set; }
        //}

        private static void GetAccess()
        {
            HttpWebRequest request = WebRequest.CreateHttp("https://id.twitch.tv/oauth2/token?"
                + "client_id=" + CLIENT_ID
                + "&client_secret=" + CLIENT_SECRET
                + "&code=" + interCode
                + "&grant_type=authorization_code"
                + "&redirect_uri=" + REDIRECT_URI);
            request.Method = "POST";

            TwitchConnectionManager.MyLogger.Log("Отправил запрос на Access Token \n");

            //AccessTokenRespond respondObject = DeserializeResponse<AccessTokenRespond>(request);
            string respond = GetJsonResponse(request);
            //accessToken = respondObject.AccessToken;
            //refreshToken = respondObject.RefreshToken;
            //accessTokenExpiresIn = respondObject.ExpriresIn;

            accessToken = FindInJson(respond, "access_token");
            refreshToken = FindInJson(respond, "refresh_token");

            TwitchConnectionManager.MyLogger.Log("получил Access Token =" + accessToken);

            // Сохранить acces token в файле 
            using (StreamWriter writer = new StreamWriter(new FileStream("keys.txt", FileMode.Create)))
            {
                writer.WriteLine("access_token " + accessToken);
                writer.WriteLine("refresh_token " + refreshToken);
            }
        }


        //private static T DeserializeResponse<T>(HttpWebRequest request) where T : ISerializableJSON
        //{
        //    string responseJSON;
        //    T responseObject;

        //    MyLogger.Log(request.RequestUri.AbsoluteUri);

        //    using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
        //    {
        //        responseJSON = reader.ReadToEnd();
        //        MyLogger.Log(responseJSON);
        //        if (responseJSON.Contains("pagination"))
        //        {
        //            TwitchConnectionManager.MyLogger.Log("raw response: " + responseJSON);
        //            responseJSON = responseJSON.Substring(0, responseJSON.LastIndexOf("pagination") - 2) + "}";
        //            TwitchConnectionManager.MyLogger.Log("response edited: " + responseJSON);
        //        }

        //        responseObject = JsonConvert.DeserializeObject<T>(responseJSON);
        //        responseObject.JSON = responseJSON;
        //    }
        //    return responseObject;
        //}

        private static string FindInJson(string json, string find)
        {
            MyLogger.Log("raw:" + json);
            string findWithQuotes = "\"" + find + "\"";
            string jsonUntilFind = json.Remove(0, json.IndexOf(findWithQuotes) + findWithQuotes.Length + 2);
            MyLogger.Log("jsonUntilFind:" + jsonUntilFind);
            string findValue = jsonUntilFind.Substring(0, jsonUntilFind.IndexOf("\""));
            MyLogger.Log("findValue:" + findValue);

            return findValue;
        }

        private static string GetJsonResponse(HttpWebRequest request)
        {
            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            }
        }
    }
}