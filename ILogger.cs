﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPIFramework
{
    public interface ILogger
    {
        void Log(string message);
        void Log(List<Tuple<string, ConsoleColor>> messagesColoured);
        void Log(string message, ConsoleColor color = ConsoleColor.Black);
    }
}
