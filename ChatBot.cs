﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using System.IO;

namespace TwitchAPIFramework
{
    public class ChatBot
    {
        public string BotName { get; private set; }
        public bool IsOnChannel { get => ChannelName != null; }
        public string ChannelName { get; private set; }
        public List<string> Viewers { get; private set; }

        TcpClient server;

        StreamReader inputStream;
        StreamWriter outputStream;

        public bool IsBotAuthorized = false;
        public bool GotPermissions = false;
        bool ToFormatMessages = false;
        public ChatBot(string nickName, string token, bool ToFormatMessages)
        {
            this.ToFormatMessages = ToFormatMessages;
            BotName = nickName;

            server = new TcpClient("irc.twitch.tv", 6667);

            inputStream = new StreamReader(server.GetStream());
            outputStream = new StreamWriter(server.GetStream());

            Thread chatThread = new Thread(Listen);
            chatThread.Start();

            Thread viewersCountDisplayThread = new Thread(() => { while (true) { Thread.Sleep(10000); if (ChannelName != null) TwitchConnectionManager.MyLogger.Log(Viewers.Count.ToString(), ConsoleColor.Cyan); } });
            viewersCountDisplayThread.Start();

            TwitchConnectionManager.MyLogger.Log("Логин: " + nickName);
            SendMessageToAPI("PASS oauth:" + token + "\n" + "NICK " + nickName);
        }

        public void Listen()
        {
            while (true)
            {
                string inputMessage = inputStream.ReadLine();
                if (inputMessage != "" && inputMessage != null)
                {

                    ProcessMessage(inputMessage);
                }
            }
        }

        public void SendMessageToAPI(string message)
        {
            TwitchConnectionManager.MyLogger.Log("<" + message, ConsoleColor.Yellow);

            outputStream.WriteLine(message);
            outputStream.Flush();
        }
        public void JoinChannel(string channelName)
        {
            if (this.ChannelName != null)
            {
                TwitchConnectionManager.MyLogger.Log("Отключаюсь от: " + this.ChannelName);
                SendMessageToAPI("PART #" + this.ChannelName);
            }

            this.ChannelName = channelName;
            Viewers = new List<string>();

            TwitchConnectionManager.MyLogger.Log("Подключаюсь к : " + this.ChannelName);
            SendMessageToAPI("JOIN #" + this.ChannelName);
        }

        public void RequestMembership()
        {
            TwitchConnectionManager.MyLogger.Log("Отправляю запрос на Membership");
            SendMessageToAPI("CAP REQ :twitch.tv/membership");
        }
        public void SendMessageToChat(string message)
        {
            TwitchConnectionManager.MyLogger.Log("Отправляю сообщение : " + message);
            SendMessageToAPI("PRIVMSG #" + ChannelName + " :" + message);
        }
        private void ProcessMessage(string inputMessage)
        {
            if (ToFormatMessages)
            {
                if (inputMessage.StartsWith(":tmi.twitch.tv") || inputMessage.StartsWith(BotName + ":tmi.twitch.tv") || inputMessage.StartsWith(":" + BotName + ".tmi.twitch.tv"))
                {
                    // Технические сообщения с кодом

                    string[] messageParts = inputMessage.Split(' ');

                    string messageBase = messageParts[0];
                    string messageCode = messageParts[1];
                    string botName = messageParts[2];
                    string messageContent = inputMessage.Remove(0, string.Join(" ", messageBase, messageCode, botName).Length + 1);

                    switch (messageCode)
                    {
                        case "353":
                            {
                                string[] viewersGroup = inputMessage.Remove(0, string.Join(" ", messageBase, messageCode, botName, "=", "#" + ChannelName).Length + 2).Split(' ');
                                Viewers.AddRange(viewersGroup);
                                foreach (var viewer in viewersGroup)
                                {
                                    TwitchConnectionManager.MyLogger.Log("+" + viewer, ConsoleColor.Green);
                                }
                                break;
                            }
                        case "366":
                            {
                                TwitchConnectionManager.MyLogger.Log("VIEWERS: " + Viewers.Count, ConsoleColor.Cyan);
                                break;
                            }
                        case "001":
                            {
                                TwitchConnectionManager.MyLogger.Log(">" + messageCode, ConsoleColor.Blue);
                                break;
                            }
                        case "002": goto case "001";
                        case "003": goto case "001";
                        case "004": goto case "001";
                        case "375": goto case "001";
                        case "372": goto case "001";
                        case "376":
                            {
                                IsBotAuthorized = true;
                                TwitchConnectionManager.MyLogger.Log(">" + messageCode, ConsoleColor.Blue);
                                TwitchConnectionManager.MyLogger.Log(">AUTHORIZED", ConsoleColor.Cyan);
                                break;
                            }
                        case "CAP":
                            {
                                GotPermissions = true;
                                TwitchConnectionManager.MyLogger.Log(">MEMBERSHIP ACQUIRED" + messageCode, ConsoleColor.Blue);
                                break;
                            }
                        default:
                            {
                                TwitchConnectionManager.MyLogger.Log("UNRECOGNIZED COMMAND: " + inputMessage, ConsoleColor.Red);
                                break;
                            }
                    }
                }
                else
                {
                    // Особенные сообщения
                    string[] commandParts = inputMessage.Split(' ');
                    if (commandParts[0] == "PING")
                    {
                        TwitchConnectionManager.MyLogger.Log(">PING", ConsoleColor.Blue);
                        SendMessageToAPI("PONG " + commandParts[1]);
                    }
                    else
                    {
                        switch (commandParts[1])
                        {
                            case "PRIVMSG":
                                {
                                    string nickname = ExtractNickname(commandParts[0]);
                                    string message = inputMessage.Substring(inputMessage.IndexOf(("#" + ChannelName)) + ("#" + ChannelName).Length + 2);

                                    //using (ViewerMessagesContext context = new ViewerMessagesContext())
                                    //{
                                    //    context.Messages.Add(new MessageData() {ChannelName = this.channelName, DateTime = DateTime.Now, Message = message, ViewerName = nickname });
                                    //    context.SaveChanges();
                                    //}

                                    List<Tuple<string, ConsoleColor>> messageParts = new List<Tuple<string, ConsoleColor>>();
                                    messageParts.Add(new Tuple<string, ConsoleColor>(nickname + ":", ConsoleColor.DarkGray));
                                    messageParts.Add(new Tuple<string, ConsoleColor>(message, ConsoleColor.Black));
                                    TwitchConnectionManager.MyLogger.Log(messageParts);
                                    TwitchConnectionManager.MyLogger.Log("\n");
                                    break;
                                }
                            case "JOIN":
                                {
                                    string nickname = ExtractNickname(commandParts[0]);
                                    TwitchConnectionManager.MyLogger.Log("+" + nickname, ConsoleColor.Green);
                                    Viewers.Add(nickname);
                                    break;
                                }
                            case "PART":
                                {
                                    string nickname = ExtractNickname(commandParts[0]);
                                    TwitchConnectionManager.MyLogger.Log("-" + nickname, ConsoleColor.DarkGreen);
                                    if (Viewers.Contains(nickname))
                                    {
                                        Viewers.Remove(nickname);
                                    }
                                    else
                                    {
                                        TwitchConnectionManager.MyLogger.Log("CANT FOUND " + nickname + " IN LIST", ConsoleColor.Red);
                                    }
                                    break;
                                }
                            default:
                                {
                                    TwitchConnectionManager.MyLogger.Log("UNRECOGNIZED COMMAND: " + inputMessage, ConsoleColor.Red);
                                    break;
                                }
                        }
                    }
                }
            }
            else
            {
                TwitchConnectionManager.MyLogger.Log(inputMessage);
            }
        }

        private string ExtractNickname(string v)
        {
            return v.Substring(1, v.IndexOf('!') - 1);
        }


    }

}
