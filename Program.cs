﻿#define j

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchAPIFramework
{
#if !LIBRARY
    public class Program
    {
        public static TwitchConnectionManager twitch;
        private static void Main()
        {
            twitch = new TwitchConnectionManager(new ConsoleLogger(), false);
            twitch.CreateBot();
            twitch.JoinChannel("maxmontana");
            //SendWave("всем драсте");
        }
        static void SendWave(string name)
        {
            double scale = 40;
            double period = 10;
            int length = 2;
            for (int c = 0; c < length; c++)
            {
                for (double i = 0; i < 2 * Math.PI; i += Math.PI / period)
                {
                    StringBuilder sb = new StringBuilder();

                    //от 0 до scale
                    int distance = (int)Math.Floor((Math.Cos(i) + 1d) * scale / 2);
                    for (int j = 0; j < distance; j++)
                    {
                        sb.Append('.');
                    }
                    sb.Append(name);
                    for (int j = 0; j < scale - distance; j++)
                    {
                        sb.Append('.');
                    }
                    twitch.SendMessageToChat(sb.ToString());
                }
            }

        }
    }
#endif
}
